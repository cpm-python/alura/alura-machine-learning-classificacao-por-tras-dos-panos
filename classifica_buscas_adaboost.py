import pandas as pd
from collections import Counter

df = pd.read_csv('buscas2_aula.csv')

x_df = df[['home', 'busca', 'logado']]
y_df = df.comprou

xDummies_df = pd.get_dummies(x_df)
yDummies_df = y_df

x = xDummies_df.values
y = yDummies_df.values


porcentagem_treino = 0.8
porcentagem_teste = 0.1

tamanho_de_treino = int(porcentagem_treino * len(y))
tamanho_de_teste = int(porcentagem_teste * len(y))
tamanho_de_validacao = int(len(y) - tamanho_de_treino - tamanho_de_teste)

treino_dados = x[0:tamanho_de_treino]
treino_marcacoes = y[0:tamanho_de_treino]

fim_de_teste = tamanho_de_treino + tamanho_de_teste
teste_dados = x[tamanho_de_treino:fim_de_teste]
teste_marcacoes = y[tamanho_de_treino:fim_de_teste]

validacao_dados = x[fim_de_teste:]
validacao_marcacoes = y[fim_de_teste:]

def fit_and_predict(nome, modelo, treino_dados, treino_marcacoes, teste_dados, teste_marcacoes):
    modelo.fit(treino_dados, treino_marcacoes)

    resultado = modelo.predict(teste_dados)

    acertos = resultado == teste_marcacoes

    total_de_acertos = sum(acertos)
    total_de_elementos = len(teste_dados)
    taxa_de_acertos = 100.0 * (total_de_acertos / total_de_elementos)
    print(f'{nome}: {total_de_acertos} acertados em {total_de_elementos}: {taxa_de_acertos}%')

    return taxa_de_acertos


from sklearn.ensemble import AdaBoostClassifier
modeloAdaBoost = AdaBoostClassifier()
resultado_adaboost = fit_and_predict('AdaBoostClassifier', modeloAdaBoost, treino_dados, treino_marcacoes, teste_dados, teste_marcacoes)

from sklearn.naive_bayes import MultinomialNB
modeloMultinomial = MultinomialNB()
resultado_multimonial = fit_and_predict('MultinomialNB', modeloMultinomial, treino_dados, treino_marcacoes, teste_dados, teste_marcacoes)

if resultado_adaboost < resultado_multimonial :
    vencedor = modeloMultinomial
else :
    vencedor = modeloAdaBoost


vencedor.fit(treino_dados, treino_marcacoes)

resultado = vencedor.predict(validacao_dados)
acertos = resultado == validacao_marcacoes

total_de_acertos = sum(acertos)
total_de_elementos = len(validacao_dados)
taxa_de_acertos = 100.0 * (total_de_acertos / total_de_elementos)
print(f'O mió de goiais: {total_de_acertos} acertados em {total_de_elementos}: {taxa_de_acertos}%')


acerto_base = max(Counter(validacao_marcacoes).values())
taxa_de_acerto_base = 100.0 * acerto_base / len(validacao_marcacoes)
print(f'Taxa de acertos base: {taxa_de_acerto_base}%')