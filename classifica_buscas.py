import pandas as pd
from collections import Counter

df = pd.read_csv('buscas2_aula.csv')

x_df = df[['home', 'busca', 'logado']]
y_df = df.comprou

xDummies_df = pd.get_dummies(x_df)
yDummies_df = y_df

x = xDummies_df.values
y = yDummies_df.values


porcentagem_treino = 0.9

tamanho_de_treino = int(porcentagem_treino * len(y))
tamanho_de_teste = int(len(y) - tamanho_de_treino)

treino_dados = x[:tamanho_de_treino]
treino_marcacoes = y[:tamanho_de_treino]

teste_dados = x[-tamanho_de_teste:]
teste_marcacoes = y[-tamanho_de_teste:]

from sklearn.naive_bayes import MultinomialNB
modelo = MultinomialNB()
modelo.fit(treino_dados, treino_marcacoes)

resultado = modelo.predict(teste_dados)
print(resultado)

acertos = resultado == teste_marcacoes

total_de_acertos = sum(acertos)
total_de_elementos = len(teste_dados)
taxa_de_acertos = 100.0 * (total_de_acertos / total_de_elementos)
print(f'{total_de_acertos} acertados em {total_de_elementos}: {taxa_de_acertos}%')

acerto_base = max(Counter(teste_marcacoes).values())
taxa_de_acerto_base = 100.0 * acerto_base / len(teste_marcacoes)
print(f'Taxa de acertos base: {taxa_de_acerto_base}%')