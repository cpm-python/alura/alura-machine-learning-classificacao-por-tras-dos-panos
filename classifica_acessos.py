from dados import carregar_acessos

x, y = carregar_acessos()

x_treino = x[:90]
print(x_treino)
y_treino = y[:90]
print(y_treino)
x_teste = x[-9:]
print(x_teste)
y_teste = y[-9:]
print(y_teste)

from sklearn.naive_bayes import MultinomialNB
modelo = MultinomialNB()
modelo.fit(x_treino, y_treino)

print(modelo.predict([[1, 0, 1], [0, 1, 1], [1, 0, 1], [1, 1, 0], [1, 1, 1]]))

resultado = modelo.predict(x_teste)
print(resultado)

diferencas = resultado - y_teste
acertos = [d for d in diferencas if d == 0]
total_acertos = len(acertos)
total_elementos = len(x_teste)

taxa_de_acertos = 100.0 * (total_acertos / total_elementos)
print(f'{total_acertos} acertados em {total_elementos}: {taxa_de_acertos}%')

